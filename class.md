### Classes[4] = "A Primer on Biological Seqs"

#### [Análise de Sequências Biológicas 2024-2025](../../index.html)

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

---

### Summary

* &shy;<!-- .element: class="fragment" -->Sequence representation reminder
* &shy;<!-- .element: class="fragment" -->"Low Throughput" Sequencing
* &shy;<!-- .element: class="fragment" -->Getting data Pt.1
* &shy;<!-- .element: class="fragment" -->File formats
* &shy;<!-- .element: class="fragment" -->Sequence databases
* &shy;<!-- .element: class="fragment" -->Getting data Pt.2
* &shy;<!-- .element: class="fragment" -->Practical examples

---

### Short reminder

![IUPAC](assets/IUPAC.png)

IUPAC codes

|||

## Nucleotides

<table BORDER CELLSPACING=0 CELLPADDING=2 COLS=2 WIDTH="600" style="font-size:50%">
<tr>
<td BGCOLOR="#B0C4DE"><font color="#000000">IUPAC nucleotide code</font></td>
<td BGCOLOR="#B0C4DE"><font color="#000000">Base</font></td>
</tr>
<tr>
<td>A</td>
<td>Adenine</td>
</tr>
<tr>
<td>C</td>
<td>Cytosine</td>
</tr>
<tr>
<td>G</td>
<td>Guanine</td>
</tr>
<tr>
<td>T (or U)</td>
<td>Thymine (or Uracil)</td>
</tr>
<tr>
<td>R</td>
<td>A or G</td>
</tr>
<tr>
<td>Y</td>
<td>C or T</td>
</tr>
<tr>
<td>S</td>
<td>G or C</td>
</tr>
<tr>
<td>W</td>
<td>A or T</td>
</tr>
<tr>
<td>K</td>
<td>G or T</td>
</tr>
<tr>
<td>M</td>
<td>A or C</td>
</tr>
<tr>
<td>B</td>
<td>C or G or T</td>
</tr>
<tr>
<td>D</td>
<td>A or G or T</td>
</tr>
<tr>
<td>H</td>
<td>A or C or T</td>
</tr>
<tr>
<td>V</td>
<td>A or C or G</td>
</tr>
<tr>
<td>N</td>
<td>any base</td>
</tr>
<tr>
<td>-</td>
<td>gap</td>
</tr>
</table>

---

### Getting data

![DNA lab](assets/DNA_lab.jpg)

---

### Sanger sequencing

* &shy;<!-- .element: class="fragment" -->[How Sanger sequencing works](https://www.youtube.com/watch?v=KTstRrDTmWI)

&shy;<!-- .element: class="fragment" -->![Chromatogram](assets/chroma.png)

|||

### Sanger sequencing

* &shy;<!-- .element: class="fragment" -->Golden Standard
* &shy;<!-- .element: class="fragment" -->High quality
* &shy;<!-- .element: class="fragment" -->Low throughput
* &shy;<!-- .element: class="fragment" -->Large files
    * &shy;<!-- .element: class="fragment" -->[Here are example trace files](assets/New_traces.zip)
    * &shy;<!-- .element: class="fragment" -->How do we read these?
        * &shy;<!-- .element: class="fragment" -->[CutePeaks](https://github.com/labsquare/CutePeaks)
        * &shy;<!-- .element: class="fragment" -->[SeqTrace](https://github.com/stuckyb/seqtrace)

---

### ~~Analysing trace files~~

* &shy;<!-- .element: class="fragment" -->Today we will use [SeqTrace](https://github.com/stuckyb/seqtrace)
* &shy;<!-- .element: class="fragment" -->Your VM needs some dependencies, though
    * &shy;<!-- .element: class="fragment" -->`python2`
    * &shy;<!-- .element: class="fragment" -->`python-gtk2`
    * &shy;<!-- .element: class="fragment" -->`libcanberra-gtk0`
    * &shy;<!-- .element: class="fragment" -->`libcanberra-gtk-module`
* &shy;<!-- .element: class="fragment" -->We will make [these trace files](assets/New_traces.zip) ready for "production"

---

### "Low Throughput" Sequence formats

* &shy;<!-- .element: class="fragment" -->Some of the most frequent file formats
* &shy;<!-- .element: class="fragment" -->[FASTA](https://zhanglab.ccmb.med.umich.edu/FASTA/)
* &shy;<!-- .element: class="fragment" -->[GB](https://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html)
* &shy;<!-- .element: class="fragment" -->[MEGA](https://www.megasoftware.net/webhelp/walk_through_mega/mega_basics_hc.htm)
* &shy;<!-- .element: class="fragment" -->[ALN](http://meme-suite.org/doc/clustalw-format.html)
* &shy;<!-- .element: class="fragment" -->[NEXUS](http://hydrodictyon.eeb.uconn.edu/eebedia/index.php/Phylogenetics:_NEXUS_Format)
* &shy;<!-- .element: class="fragment" -->[PHYLIP](http://www.phylo.org/tools/obsolete/phylip.html)
* &shy;<!-- .element: class="fragment" -->...

|||

### "Low Throughput" Sequence formats

* [Sequence storage files](assets/unaligned_seqs.tar.xz)
* [Sequence alignment files](assets/aligned_seqs.tar.xz)

---

### There must be a better way!

[![AliView](assets/aliview.png)](https://ormbunkar.se/aliview/)


* &shy;<!-- .element: class="fragment" -->First, install Java:
    * &shy;<!-- .element: class="fragment" -->`sudo apt update`
    * &shy;<!-- .element: class="fragment" -->`sudo apt install openjdk-8-jre`
* &shy;<!-- .element: class="fragment" -->Then, get *AliView* from [here](https://ormbunkar.se/aliview/downloads/linux/linux-version-1.28/) and install it
* &shy;<!-- .element: class="fragment" -->Finally, open the sequence files from the last slide with *AliView* and explore them

---

### DNA sequence databases

* &shy;<!-- .element: class="fragment" -->[NCBI (USA)](https://www.ncbi.nlm.nih.gov/)
* &shy;<!-- .element: class="fragment" -->[EMBL (Europe)](https://www.embl.org/)
* &shy;<!-- .element: class="fragment" -->[DDBJ (Japan)](https://www.ddbj.nig.ac.jp)
    * &shy;<!-- .element: class="fragment" -->Data repositories
    * &shy;<!-- .element: class="fragment" -->Replicated
    * &shy;<!-- .element: class="fragment" -->Queryable

&shy;<!-- .element: class="fragment" -->![Data center picture](assets/data_center.jpg)

---

### Obtaining sequences

&shy;<!-- .element: class="fragment" -->![Picture of Psammodromus algirus](assets/psammodromus.jpg)

* &shy;<!-- .element: class="fragment" -->Meet *Psammodromus algirus*
    * &shy;<!-- .element: class="fragment" -->Go to the [NCBI](https://www.ncbi.nlm.nih.gov/) website
    * &shy;<!-- .element: class="fragment" -->Select the "nucleotide" database
    * &shy;<!-- .element: class="fragment" -->Search for *Psammodromus algirus[organism], cytb[gene]*
* &shy;<!-- .element: class="fragment" -->Click "Send to" and get a file with all the sequences in FASTA format
    * &shy;<!-- .element: class="fragment" -->Save it as `~/sequences/P_algirus_NCBI.fasta`

---

### Using the NCBI Entrez API

* &shy;<!-- .element: class="fragment" -->Called [E-utilities](https://www.ncbi.nlm.nih.gov/books/NBK25501/) or `eutils`
* &shy;<!-- .element: class="fragment" -->Comprised of several functions like:
    * &shy;<!-- .element: class="fragment" -->[esearch](https://www.ncbi.nlm.nih.gov/books/NBK25499/#chapter4.ESearch)
    * &shy;<!-- .element: class="fragment" -->[epost](https://www.ncbi.nlm.nih.gov/books/n/helpeutils/chapter4/#chapter4.EPost)
    * &shy;<!-- .element: class="fragment" -->[esummary](https://www.ncbi.nlm.nih.gov/books/NBK25499/#_chapter4_ESummary_)
    * &shy;<!-- .element: class="fragment" -->[efetch](https://www.ncbi.nlm.nih.gov/books/n/helpeutils/chapter4/#chapter4.EFetch)
* &shy;<!-- .element: class="fragment" -->Today we will only use `esearch` and `efetch`
* &shy;<!-- .element: class="fragment" -->Base address: [https://eutils.ncbi.nlm.nih.gov/entrez/eutils](https://eutils.ncbi.nlm.nih.gov/entrez/eutils)
    * &shy;<!-- .element: class="fragment" -->[Try it in your browser!](https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi/?db=nucleotide&term=Psammodromus%20algirus[organism],cytb[gene])
    * &shy;<!-- .element: class="fragment" -->If you don't like XML, you can ask for other formats, like JSON with `&retmode=json`

|||

### Now try it from your shell!

```bash
# SPOILER ALERT!
# Scroll below for the solution














wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi/\?db\=nucleotide\&term\="Psammodromus algirus[organism],cytb[gene]" -O ~/Pal_Ids.xml
```

---

### What about getting some data?

* &shy;<!-- .element: class="fragment" -->In order to get data, we use `efetch`
* &shy;<!-- .element: class="fragment" -->`efetch` takes "arguments" such as a database, a list of IDs, and the desired type of return:
    * &shy;<!-- .element: class="fragment" -->https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi/?db=nucleotide&id=1252327351,1252327349,1252327347&rettype=fasta
* &shy;<!-- .element: class="fragment" -->This API call will retrieve 3 sequences in FASTA format
* &shy;<!-- .element: class="fragment" -->Can you adapt it to get the full ID list from the *P. algirus* query?
* &shy;<!-- .element: class="fragment" -->Note that by *default* NCBI will only return the first 20 IDs
   * &shy;<!-- .element: class="fragment" -->Use `&retmax=100` to work around the issue


|||

### A more dynamic approach

```bash
# SPOILER ALERT!
# Scroll below for the solution














ids=$(wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi/\?db\=nucleotide\&term\="Psammodromus algirus[organism], cytb[gene]"\&retmax=100 -O - |grep -i "^<Id>" | sed 's/[^0-9]//g' | tr "\n" "," |rev |cut -c 2- |rev)  # Get the list of IDs, comma separated
wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi/\?db\=nucleotide\&id\=$ids\&rettype\=fasta -O ~/P_algirus_cytb.fasta
```

---

### Using the "history" feature

* &shy;<!-- .element: class="fragment" -->The "history" feature will store an ID list on NCBI's servers
* &shy;<!-- .element: class="fragment" -->This is useful if:
    * &shy;<!-- .element: class="fragment" -->We have a particularly large list of IDs
    * &shy;<!-- .element: class="fragment" -->We want to further manipulate the search results
    * &shy;<!-- .element: class="fragment" -->We want to create an "artificial" ID list
* &shy;<!-- .element: class="fragment" -->*Entrez*'s "history" will provide us with 2 variables:
    * &shy;<!-- .element: class="fragment" -->A *WebEnv*
    * &shy;<!-- .element: class="fragment" -->A *Query Key*
* &shy;<!-- .element: class="fragment" -->Just pass `&usehistory=y` to `esearch`

|||

### History in practice

```bash
wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi\?db\=nucleotide\&term\=Psammodromus%20algirus\[organism\],cytb\[gene\]\&usehistory\=y -O ~/Pal_history.xml
```

* Find both the *WebEnv* and the *Query Key* variables in the resulting XML

---

### Using "history" part II

* &shy;<!-- .element: class="fragment" -->Now that we have a stored list of IDs, how do we access it?
* &shy;<!-- .element: class="fragment" -->Using `efetch`, with the following parameters:
    * &shy;<!-- .element: class="fragment" -->`db=nucleotide`
    * &shy;<!-- .element: class="fragment" -->`usehistory=y`
    * &shy;<!-- .element: class="fragment" -->`query_key=${key}`
    * &shy;<!-- .element: class="fragment" -->`WebEnv=${webenv}`
    * &shy;<!-- .element: class="fragment" -->`rettype=fasta`
* &shy;<!-- .element: class="fragment" -->Try it!

---

### Homework:

* &shy;<!-- .element: class="fragment" -->Write a small, non-interactive script to retrieve sequences that:
    * &shy;<!-- .element: class="fragment" -->Uses the *Entrez* API
    * &shy;<!-- .element: class="fragment" -->Allows the user to choose which database to query
    * &shy;<!-- .element: class="fragment" -->Allows the user to pass a search term
    * &shy;<!-- .element: class="fragment" -->Takes user input as **command line arguments**
    * &shy;<!-- .element: class="fragment" -->Uses the "history" API feature
    * &shy;<!-- .element: class="fragment" -->The result has to be in FASTA format and written to `STDOUT`
* &shy;<!-- .element: class="fragment" -->Programming language and tools are up to you
* &shy;<!-- .element: class="fragment" -->Make sure each part of your program is an independent function
* &shy;<!-- .element: class="fragment" -->Document **each function**, and **program usage**!
* &shy;<!-- .element: class="fragment" -->Organize in groups of 4 elements
* &shy;<!-- .element: class="fragment" -->Delivery via public git repository (with commit hash!)
* &shy;<!-- .element: class="fragment" style="color:red"-->Deadline: 10/03/2025 23:59

---

### References

* [IUPAC reference](https://www.bioinformatics.org/sms/iupac.html)
* [How Sanger sequencing works](https://www.thermofisher.com/us/en/home/life-science/sequencing/sanger-sequencing/sanger-sequencing-workflow.html)
* [NCBI search terms list](https://www.ncbi.nlm.nih.gov/books/NBK49540/)
* [E-utilities quick start guide](https://www.ncbi.nlm.nih.gov/books/NBK25500/)
* [E-utilities full documentation](https://eutils.ncbi.nlm.nih.gov/)
* [Python urllib](https://docs.python.org/3/library/urllib.html)
* [Python requests](https://docs.python-requests.org/en/latest/)
