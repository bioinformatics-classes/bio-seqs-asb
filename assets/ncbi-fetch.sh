#!/bin/bash

# Usage: ncbi-fecth.sh "search term" "database to search"

eRes=$(wget https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi\?db\=${2}\&term\="${1}"\&usehistory\=y\&retmode\=json -O -)

QKey=$(echo ${eRes}|cut -d "," -f 6|cut -d ":" -f 2|tr -d "\"" |tr -d " ")
WEnv=$(echo ${eRes}|cut -d "," -f 7|cut -d ":" -f 2|tr -d "\"" |tr -d " ")

FETCH_URL="https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi/?db=${2}&query_key=${QKey}&WebEnv=${WEnv}&rettype=fasta"

wget $FETCH_URL -O -

